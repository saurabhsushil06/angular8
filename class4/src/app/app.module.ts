import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { WorningAlertComponent } from './worning-alert/wornig-alert-module';
import { SucessAlertComponentComponent } from './sucess-alert-component/sucess-alert-component.component';
import { from } from 'rxjs';
import { AddbuttonComponent } from './addbutton/addbutton.component';

@NgModule({
  declarations: [
    AppComponent,
    WorningAlertComponent,
    SucessAlertComponentComponent,
    AddbuttonComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
