import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceItemComponent } from './invoice-item/invoice-item.component';
import { InvoideHomeComponent } from './invoide-home/invoide-home.component';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceReportComponent } from './invoice-report/invoice-report.component';


const routes: Routes = [
  {
    path: '', component: InvoideHomeComponent
    , children: [
      {
        path: '', component: InvoiceItemComponent
      },
      {
        path: 'invoiceItem', component: InvoiceItemComponent
      },
      {
        path: 'invoiceReport', component: InvoiceReportComponent
      }]
  }
];

@NgModule({
  declarations: [InvoiceItemComponent, InvoideHomeComponent, InvoiceReportComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class InvoiceModule { }
