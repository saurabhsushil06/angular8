import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoideHomeComponent } from './invoide-home.component';

describe('InvoideHomeComponent', () => {
  let component: InvoideHomeComponent;
  let fixture: ComponentFixture<InvoideHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoideHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoideHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
