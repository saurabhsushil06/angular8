import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HelpComponent } from './help/help.component';


const routes: Routes = [{
  path: '', component: HelpComponent
},
{
  path: 'invoice', loadChildren: () => import('./invoice/invoice.module').then(m => m.InvoiceModule)
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
