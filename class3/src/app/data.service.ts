
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService{
  constructor() { }
  createDb(){

    let  users =  [
      {  id:  1,  name:  'User 1' },
      {  id:  2,  name:  'User 2' }
           
    ];
    let products =[
      {
        id:1, name:"Product 1", img:"assets/images/profile-img.jpg"
      },
      {
       id:2, name:"Product 2", img:"assets/images/profile-img.jpg"
     }
    ];

    return { users, products};

   }
}