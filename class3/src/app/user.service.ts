import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  API_URL: string = "api/";
  http: any;
  constructor(private httpClient: HttpClient) { }

  getUsers() {
    return this.httpClient.get(this.API_URL + 'users')
  }

  getProduct(userId) {
    return this.httpClient.get(`${this.API_URL + 'users'}/${userId}`)
  }

  getProducts(){
    debugger;
    return this.httpClient.get(this.API_URL + 'products')
  }

 

}
