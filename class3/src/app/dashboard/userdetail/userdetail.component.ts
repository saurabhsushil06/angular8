import { Component, OnInit } from '@angular/core';
import { Users } from '../users';
import { UserService } from 'src/app/user.service';

import { from } from 'rxjs';
import { ModalService } from 'src/app/_modal/modal.service';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css']
})
export class UserdetailComponent implements OnInit {
  bodyText: string;
  powers=[];

  constructor(private user:UserService, private modalService:ModalService) { }
  userslist: Users[] = [];
  model= {name:""};

  openModal(id: string, uid:number) {
    this.modalService.open(id);
}

closeModal(id: string) {
    this.modalService.close(id);
}

  ngOnInit() {
    this.powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];

    this.model.name="sushil";
    this.bodyText = 'This text can be updated in modal 1';
    this.user.getUsers().subscribe((a : Users[]) => {

  this.userslist = a;
  //console.log(a);

    })

  }

}
