import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { RouterModule, Routes } from '@angular/router';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { UsereditComponent } from './userdetail/useredit/useredit.component';
import { ModalModule } from '../_modal/modal.module';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path:'',
    component:MainComponent,
    children:[
      {
        path:'userdetail',
        component:UserdetailComponent
      },
      {
        path:'productdetail',
        component:ProductdetailComponent
      },
      {
        path:'productdetail/id',
        component:ProductdetailComponent
      }

    ]
  }
 ];

@NgModule({
  declarations: [MainComponent, UserdetailComponent, ProductdetailComponent, UsereditComponent],
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
