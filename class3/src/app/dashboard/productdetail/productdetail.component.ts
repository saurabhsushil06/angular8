import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { Products } from '../products';


@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.css']
})
export class ProductdetailComponent implements OnInit {
productList :Products[] = [];
  constructor(private  products:UserService) { }

  ngOnInit() {
    debugger;
    this.products.getProducts().subscribe((p : Products[]) => {
      this.productList =p;
    })
  }

}
