import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductComponent } from './product/product.component';
import { UserhomeComponent } from './user/userhome/userhome.component';


const routes: Routes = [
  {
    path:'',
    component:DashboardComponent
  },
  {
    path: 'userdetail', 
    loadChildren: () => import('./user/user.module').then(m => m.UserModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
function newFunction(): string {
  return 'userhome';
}

