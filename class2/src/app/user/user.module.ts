import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { UserhomeComponent } from './userhome/userhome.component';
import { DashboardComponent } from '../dashboard/dashboard.component';



const routes: Routes = [
  {
    path: '',
    component:DashboardComponent,
    children: [
     {path: 'userdetail', 
       component: UserdetailComponent,
       //loadChildren: () => import('./home/user/user.component').then(m => m.UserComponent)
     }
      
     ]
  }
  
];


@NgModule({
  declarations: [UserdetailComponent, InvoiceComponent, UserhomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserModule { }
