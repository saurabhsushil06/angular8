import { Injectable } from '@angular/core';
import { UserItem } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userlist: UserItem[] = [{
    id: 1,
    name: 'Krunal',
    img:''
},
{
    id: 2,
    name: 'Rushabh',
    img:''
},
{
    id: 3,
    name: 'Ankit',
    img:''
}];

  constructor() { }
}
