import { Injectable } from '@angular/core';
import { Product } from './product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  students: Product[] = [{
    id: 1,
    name: 'Krunal',
    img:''
},
{
    id: 2,
    name: 'Rushabh',
    img:''
},
{
    id: 3,
    name: 'Ankit',
    img:''
}];

  constructor() { }
}
