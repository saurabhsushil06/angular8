import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

 mainmenu = [
   {
     name:"Home",
     id:1,
     url:"/"
   },
   {
     name:"About",
     id:2,
     url:"about"
   },
   {
     name:"Service",
     id:3,
     url:"service"
   }
 ]
  
}
