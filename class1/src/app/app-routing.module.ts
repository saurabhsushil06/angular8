import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent } from "./home/home.component";
import {AboutComponent } from "./about/about.component";
import { from } from 'rxjs';
import { UserComponent } from './home/user/user.component';

const routes: Routes = [
  { path: '', 
  component: HomeComponent
  
},
{
  path: '',
  component: HomeComponent,
  children: [
    {path: 'user', 
    component: UserComponent,
    //loadChildren: () => import('./home/user/user.component').then(m => m.UserComponent)
  }
    
  ]
},
  { path: 'about', 
  component: AboutComponent
  
}

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
