import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { UserComponent } from '../home/user/user.component';

@Component({
  selector: 'app-leftnavbar',
  templateUrl: './leftnavbar.component.html',
  styleUrls: ['./leftnavbar.component.scss']
})
export class LeftnavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
