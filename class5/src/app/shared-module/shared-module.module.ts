import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { UserComponent } from '../dashboard/user/user.component';
import { DateformatePipe } from './dateformate.pipe';
import { AphDirective } from './aph.directive';
import { HighlightDirective } from './highlight.directive';
 
 


@NgModule({
  declarations: [UserComponent, DateformatePipe, AphDirective, HighlightDirective],
  imports: [
    CommonModule
  ],
  exports:[UserComponent, AphDirective, HighlightDirective]
})
export class SharedModuleModule { 
  
}
