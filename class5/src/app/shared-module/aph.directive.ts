import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAph]'
})
export class AphDirective {

  constructor(el: ElementRef) {
    el.nativeElement.innerText= "Text is changed by changeText Directive.";
    el.nativeElement.style.backgroundColor= "red";
   }

}
