import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Pipe({
  name: 'dateformate'
})
export class DateformatePipe implements PipeTransform {

  transform(value: any, ...args: any[]) {
    let d = new Date(value)
     return moment(d).format('DD/MM/YYYY')

  }

}
