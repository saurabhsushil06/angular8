import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './user/user.component';
import { SharedModuleModule } from '../shared-module/shared-module.module';
 
 
const routes: Routes = [
  {
    path:"",
    component:UserlistComponent
  } 
];

@NgModule({
  declarations: [UserlistComponent],
  imports: [
    CommonModule,HttpClientModule,SharedModuleModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
