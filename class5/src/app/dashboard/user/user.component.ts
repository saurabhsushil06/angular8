import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { DataService } from 'src/app/data.service';
 

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
@Input() user:Employee
@Input() showAll: boolean
selectedUsers:Employee
@Output() popuser = new EventEmitter<Employee>()
 
constructor() { }

  ngOnInit() {
  }

  showDetail(selectedUser:Employee){ 
  this.popuser.emit(selectedUser);
  }
}
