import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/data.service';


@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
  color: string;
  //color= "red";
  violet ='red';
  age:number;

  employees:Employee[]
  selecteduser:Employee
  myModal = false;
  constructor(private userService:DataService) { 
    this.employees = this.userService.getEmployee();
  }
 
  

  ngOnInit() {
  }

  showSelectedUservalue(user:Employee){ 
   this.myModal=true;
   this.selecteduser=user;
   this.age = this.getAge(this.selecteduser.dateOfBirth);
  }

  hide(){
    this.myModal=false;
  }

  getAge(dateOfBirth){
    return this.userService.getYear(dateOfBirth);
  }

}
