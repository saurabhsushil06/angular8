import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { error } from 'util';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-regform',
  templateUrl: './regform.component.html',
  styleUrls: ['./regform.component.css']
})
export class RegformComponent implements OnInit {
  registrationForm: FormGroup;
  submited=false;
  age="0";
 addeedItem = [];
 emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"; 
 sucessMsg = false;
 errorMsg = false;

 employees:Employee[]
 selecteduser:Employee


  constructor(private fb :FormBuilder, private ageservice: DataService, private router: Router) {
    if(localStorage.getItem('regitems')!=null){
      this.addeedItem=JSON.parse(localStorage.getItem('regitems')); 
    }
    
    }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      firstName:['', Validators.required],
      lastName:['', Validators.required],
      email:['', [Validators.required, Validators.pattern(this.emailPattern)]],
      webSite:['', Validators.required],
      dateOfBirth:['', Validators.required]
    })
  }


  
  onSubmit(){
    this.submited=true;
    if(!this.registrationForm.invalid){
    console.log(this.registrationForm.value);
    //var string = '{"items":[{"Desc":"Item1"},{"Desc":"Item2"}]}';
 
    var obj = this.registrationForm.value;
    if(this.addeedItem!=null){
    obj.id=this.addeedItem.length+1;
    }
     this.addeedItem.push(obj);
    localStorage.setItem('regitems', JSON.stringify(this.addeedItem));
    this.sucessMsg=true;
    this.errorMsg=false;
    this.submited=false;
    this.registrationForm.reset();
    this.age="0";
    this.router.navigate(['/dashboard']);

  }else{
    console.log("This is invalid"); 
    console.log(this.registrationForm.controls);
    this.errorMsg=true;
    this.sucessMsg=false;
  }
  }
get f(){
  return this.registrationForm.controls;
}

myfun(){
this.age = this.ageservice.gateDateofBirth(this.registrationForm.controls.dateOfBirth.value);
}


regGet(user:Employee){
  this.selecteduser=user; 
}
 

}
