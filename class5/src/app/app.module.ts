import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RegformComponent } from './regform/regform.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DataService } from './data.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import { UserComponent } from './dashboard/user/user.component';
import { SharedModuleModule } from './shared-module/shared-module.module';
 
 
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegformComponent  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    DlDateTimeDateModule,
    DlDateTimePickerModule,
    SharedModuleModule 
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
