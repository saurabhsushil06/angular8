import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegformComponent } from './regform/regform.component';
import { HeaderComponent } from './header/header.component';


const routes: Routes = [

  {
    path:"",
    component:RegformComponent
  },
  {
    path: 'dashboard', 
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
