import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { Observable ,from } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
export class DataService {

  API_URL: string = "http://dummy.restapiexample.com/api/v1/";

  http: any;
  constructor(private httpClient: HttpClient) { }

  getEmployee() {
    let x = localStorage.getItem('regitems');
    return JSON.parse(x);
    //return this.httpClient.get(this.API_URL + 'employees');
  }

  getProduct(userId) {
    return this.httpClient.get(`${this.API_URL + 'users'}/${userId}`)
  }

  gateDateofBirth(DOB:any){
    var age = moment().diff(DOB, 'year') ;
    var m = moment().diff(DOB, 'month') % 12;
    var fullage = age + " Years" + " , " + m + " Months";
    return fullage;
  }

getYear(DOB:any){
  return moment().diff(DOB, 'year') ;
}

}
